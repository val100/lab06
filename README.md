# Ansible - Workshop
Lab 06: Variables

---
# Preperations

## Prepare hosts file - open host file

```
$ sudo vim /etc/ansible/hosts
```

## Paste the following snippet in the file - nodeip will be replaced by the managed node ip(or ip's) - <bold>if the hosts file is already updated, skip this step</bold>

```
[demoservers]
(nodeip(s))
```

## Save and quit
```
press esc
press :wq
```

# Instructions

- Create a variables file
- Create a playbook to call the variable file and echo the variables
- Run playbook
- Add a fact variable to echo - node ip
- Run playbook

## Create a variables file

### Create file
```
$ sudo vim var.yml
press i
```

### Paste the next snippet
```
---
http_port: 80
database_server: storage.example.com
```

### Save and quit
```
Press esc
press :wq
```

## Create a playbook to call the variable file and echo the variables

### Create file
```
$ sudo vim varplaybook.yml
press i
```

### Insert next snippet
```
---
- hosts: demoservers
  vars_files:
          - ~/var.yml
  remote_user: sela
  tasks:
  - name: call vars
    debug:
            msg: "vars are {{ http_port }} and {{ database_server }}"


```
### Save and quit
```
press esc
press :wq
```

## Run the playbook

```
$ ansible-playbook varplaybook.yml
```

## Add a fact variable - node ip

### Open varplaybook.yml file
```
$ sudo vim varplaybook.yml
press i
```

### Add the next snippet to the msg part
```
 The node name is: {{ ansible_nodename }}, Node IP is : {{ ansible_default_ipv4.address }} 
```

## Run playbook
```
ansible-playbook varplaybook.yml
```


